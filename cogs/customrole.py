import discord
from discord.ext.commands import Cog
from discord.ext import commands


class CustomRole(Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.command(aliases=["sar", "lsar", "assignable"])
    @commands.cooldown(1, 30, commands.BucketType.channel)
    async def assignableroles(self, ctx):
        """Gets the self-assignable roles of the guild.

        This list can only be retrieved every 30 secs per channel."""
        sar = self.bot.query_setting("custom", ctx.guild.id)
        swooshy_sar = ", ".join(sar)
        await ctx.send(
            f"{len(sar)} custom role(s):\n"
            f"```\n{swooshy_sar}```\n"
            "Use `assignrole <role_name>` command to "
            "assign one to yourself."
        )

    @commands.bot_has_permissions(manage_roles=True)
    @commands.guild_only()
    @commands.command(aliases=["asar", "rsar", "assign"])
    @commands.cooldown(1, 10, commands.BucketType.user)
    async def assignrole(self, ctx, *, role_name: str, user: discord.Member = None):
        """Gives you a custom role.

        Also, you can use the syntax "rb!assignrole <role> <user>" if you have
        Manage Roles permission to set another user's role.

        This command can only be used every 10 seconds per user."""

        if not user:
            user = ctx.author
        user_has_perms = await self.bot.owner_or_manage_roles(ctx)
        if user != ctx.author and not user_has_perms:
            return await ctx.send(
                f"{ctx.author.mention}: "
                "To be able to set others' "
                "roles, you need Manage Roles permission."
            )

        sar = self.bot.query_setting("custom", ctx.guild.id)
        if role_name not in sar:
            return await ctx.send(
                f"{ctx.author.mention}: "
                "This isn't a valid custom role "
                "for this guild."
            )

        role = await self.bot.get_role(ctx, role_name, False)
        if role in user.roles:
            await user.remove_roles(role)
            return await ctx.send(f"{user.mention}: Removed role.")

        await user.add_roles(role)
        safe_name = str(ctx.author).replace("@", "")
        await ctx.send(f"{safe_name}: Role set!")


def setup(bot):
    bot.add_cog(CustomRole(bot))
